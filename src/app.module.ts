import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EmailController } from './email/email.controller';
import { ConfigModule } from '@nestjs/config';
import { SendGridService } from './email/sendgrid.service';

@Module({
  imports: [ConfigModule.forRoot()],
  controllers: [AppController, EmailController],
  providers: [AppService, SendGridService],
})
export class AppModule {}
