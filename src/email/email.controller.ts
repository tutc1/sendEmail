import { Controller, Get, Post, Query } from '@nestjs/common';
import { SendGridService } from 'src/email/sendgrid.service';
@Controller('email')
export class EmailController {
  constructor(private sendgridService: SendGridService) {}

  // @Get('text-email')
  // async plainTextEmail(@Query('toEmail') toEmail) {
  //   await this.mailservice.sendMail({
  //     to: toEmail,
  //     from: 'tongcongtu01@gmail.com',
  //     subject: 'Test SendGrid',
  //     text: 'Welcome to test mailer ',
  //   });
  //   return 'success';
  // }

  // @Post('send')
  // async sendEmail(@Query('email') email) {
  //   // const templatePath = 'src/view/confirmation-email.hbs'; // đường dẫn tới file .hbs
  //   // const html = await fs.readFile(templatePath, 'utf8');
  //   const mail = {
  //     to: email,
  //     subject: ' NestJS SendGrid',
  //     from: 'tongcongtu01@gmail.com',
  //     text: 'Hello World from NestJS SendGrid',
  //     template: '/view/confirmation-email.hbs',
  //     context: {
  //       appName: 'My App',
  //     },
  //   };
  //   return await this.sendgridService.send(mail);
  // }
  @Get()
  async sendWelcomeEmail() {
    const email = 'tutc@tokyotechlab.com';
    const name = 'congtu';
    const templateName = 'confirmation-email'; // Tên file HBS trong thư mục templates
    const subject = 'Welcome ';

    const data = {
      appName: name,
    };

    const result = await this.sendgridService.send(
      email,
      subject,
      templateName,
      data,
    );
    return result ? 'Email sent!' : 'Failed to send email.';
  }
}
