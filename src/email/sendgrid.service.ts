import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as SendGrid from '@sendgrid/mail';
import * as fs from 'fs';
import * as path from 'path';
import * as handlebars from 'handlebars';

@Injectable()
export class SendGridService {
  constructor(private readonly configService: ConfigService) {
    SendGrid.setApiKey(this.configService.get<string>('SEND_GRID_KEY'));
  }

  async send(
    to: string,
    subject: string,
    templateName: string,
    data: any,
    attachments?: any,
  ): Promise<boolean> {
    try {
      const html = await this.generateTemplate(templateName, data);
      const msg = {
        to,
        from: process.env.EMAIL_FROM,
        subject,
        html,
        attachments,
      };
      const result = await SendGrid.send(msg);
      console.log(result);
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  }
  async generateTemplate(templateName: string, data: any): Promise<string> {
    const templatePath = path.join(
      __dirname,
      '..',
      'src/templates',
      `${templateName}.hbs`,
    );
    const file = fs.readFileSync(templatePath, 'utf8');
    const template = handlebars.compile(file);

    return template(data);
  }
}
