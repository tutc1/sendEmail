export interface IEmailOptions {
  sendBefore?: Date;
  sendAfter?: Date;
}
export interface IEmailDto<T> {
  from: string;
  to: string;
  bcc?: string[];
  cc?: string[];
  templateId: string;
  payload: T;
  options?: IEmailOptions;
}
